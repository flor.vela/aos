#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main( int argc, char **argv )
{
	int fd;

	// open read-only file
	if ( ( fd = open( "test.txt", O_RDONLY ) ) == -1 ) {
		perror( "Could not open file" );

	// change permisions
	} else if ( fchmod( fd, S_IRUSR | S_IWUSR ) == -1 ) {
		perror( "Could not fchmod" );

	} else {
		printf("Changed permisions successfully!\n");      	
	}
	// close file 
	close( fd );
}