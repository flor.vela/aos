#include <stdio.h>
#include <unistd.h>
#include <signal.h>

static void alarmHandler(int signo);

int main(void){

	// system call to alarm
    alarm(10);
    // if alarm ends, then execute alarmhandler
    signal(SIGALRM, alarmHandler);
    
    // print numbers
    int i = 0;
    for(;;){
        printf("%d\n", i); 
        i++;
    }
}

static void alarmHandler(int signo){
    exit(0);
}


