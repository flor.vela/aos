import psutil

def get_processes():
	'''
	Get list of running process
	'''
	listOfProcObjects = []
	# Iterate over the list
	for proc in psutil.process_iter():
		try:
			# Fetch process details as dict
			pinfo = proc.as_dict(attrs=['pid', 'name', 'username', 'create_time'])
			pinfo['memory_usage'] = proc.memory_info().vms / (1024 * 1024)
			# Append dict to list
			listOfProcObjects.append(pinfo)

		except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
			pass

	return listOfProcObjects

def order_by_age(listOfProcObjects):
	# Sort list of dict by key created time
	listOfProcObjects = sorted(listOfProcObjects, key=lambda procObj: procObj['create_time'])
	return listOfProcObjects

def order_by_size(listOfProcObjects):
	# Sort list of dict by key memory usage 
	listOfProcObjects = sorted(listOfProcObjects, key=lambda procObj: procObj['memory_usage'], reverse=True)
	return listOfProcObjects

def print_processes(processes):
	'''
	Print processes information
	'''
	for process in processes:
		print('----------------------------------------------')
		print('pid: ' + str(process['pid'])) 
		print('name: ' + str(process['name'])) 
		print('username: ' + str(process['username'])) 
		print('create_time: ' + str(process['create_time'])) 
		print('memory_usage: ' + str(process['memory_usage'])) 
	print('\n') 	

def main():
	print('\n') 

	# get list of all processes 
	processes = get_processes()

	print('showing top 5 processes that use the most memory: ')
	# order processes
	processes_by_size = order_by_size(processes)
	# print processes
	print_processes(processes_by_size[:5])

	print('showing top 5 oldest processes: ')
	# order processes
	processes_by_age = order_by_age(processes)
	# print processes
	print_processes(processes_by_age[:5])

main()