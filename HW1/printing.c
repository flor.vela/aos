# include <sys/types.h>
# include <unistd.h>

int main(void)
{
	printf("PID: %d\n", getpid());
	printf("PPID: %d\n", getppid());
	printf("UID: %d\n", getuid());
	printf("GID: %d\n", getgid());
	exit(0);
}