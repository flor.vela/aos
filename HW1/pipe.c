#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

/* max  receiving buffer size; Note: no check or enforcement is made on this value*/
#define BUF_SIZE 256

int main()
{
    int pfd1[2];

    ssize_t numRead = -1;
    /* Note: working under the assumption that the messages are of equal length*/
    char* msg = "Somos Sistemas Operativos Avanzados";

    const unsigned int len = strlen(msg) + 1;

    char buf[BUF_SIZE];

    pipe(pfd1);
    printf("Piped opened with success. Forking ...\n");

    // child 1
    if (fork() == 0) {
        printf("\nProcess A executing...\n");
        /* close reading end of first pipe */
        close(pfd1[0]);
        /* write to pipe */
        write(pfd1[1], msg, len);
        close(pfd1[1]); 
        printf("Exiting process A...\n");
        exit(0);
    }


    // child 2
    if (fork() == 0) {
        printf("\nProcess B executing...\n");
        /* close writing end of first pipe */
        close(pfd1[1]);
        /* read from the first pipe */
        read(pfd1[0], buf, len);
        close(pfd1[0]);
        printf("Message received process B: %s\n", buf);
        printf("Exiting process B...\n");
        exit(0);
    }

    printf("Parent closing pipes.\n");
    close(pfd1[0]);
    close(pfd1[1]) ;

    printf("Parent waiting for children completion...\n");
    wait(NULL);
    wait(NULL);

    printf("Parent finishing.\n");
    exit(0);
}