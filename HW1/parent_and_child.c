# include <sys/types.h>
# include <unistd.h>
# include <stdio.h>

int main(void)
{
	pid_t pid;
	int status;
	
	printf("just one process so far \n");
	printf("Calling fork .....\n");
	
	pid = fork();
	
	if ( pid == 0 ){
		printf("Im the child: %d ", getpid());
		printf("and my parent is: %d \n", getppid());
	} else if (pid > 0) {
		wait( &status );
		printf("Im the parent: %d ", getpid());
		printf("and my child is: %d \n", pid);
	} else {
		printf("fork returned error code, no child \n");
	}
}